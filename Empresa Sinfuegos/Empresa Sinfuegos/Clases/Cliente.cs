﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa_Sinfuegos.Clases
{
    class Cliente
    {
        //id, cédula, nombres, apellidos, celular, correo, dirección, municipio.departamento
        int id;
        string cedula;
        string nombres;
        string apellidos;
        string celular;
        string correo;
        string direccion;
        string municipio;
        string departamento;

        public Cliente(int id, string cedula, string nombres, string apellidos, string celular, string correo, string direccion, string municipio, string departamento)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.celular = celular;
            this.correo = correo;
            this.direccion = direccion;
            this.municipio = municipio;
            this.departamento = departamento;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Municipio
        {
            get
            {
                return municipio;
            }

            set
            {
                municipio = value;
            }
        }

        public string Departamento
        {
            get
            {
                return departamento;
            }

            set
            {
                departamento = value;
            }
        }
    }
}
