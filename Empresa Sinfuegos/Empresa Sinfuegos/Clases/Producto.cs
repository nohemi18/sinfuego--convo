﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa_Sinfuegos.Clases
{
    class Producto
    {
        //d, categoría, tipo_extinguidor, marca, capacidad, unidad_medida, lugar_designado, fecha_recarga
        int id;
        string categoria;
        string tipo_extinguidor;
        string marca;
        string capacidad;
        string unidad_medida;
        string lugar_destinado;
        string fecha_recarga;

        public Producto(int id, string categoria, string tipo_extinguidor, string marca, string capacidad, string unidad_medida, string lugar_destinado, string fecha_recarga)
        {
            this.id = id;
            this.categoria = categoria;
            this.tipo_extinguidor = tipo_extinguidor;
            this.marca = marca;
            this.capacidad = capacidad;
            this.unidad_medida = unidad_medida;
            this.lugar_destinado = lugar_destinado;
            this.fecha_recarga = fecha_recarga;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string Tipo_extinguidor
        {
            get
            {
                return tipo_extinguidor;
            }

            set
            {
                tipo_extinguidor = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Unidad_medida
        {
            get
            {
                return unidad_medida;
            }

            set
            {
                unidad_medida = value;
            }
        }

        public string Lugar_destinado
        {
            get
            {
                return lugar_destinado;
            }

            set
            {
                lugar_destinado = value;
            }
        }

        public string Fecha_recarga
        {
            get
            {
                return fecha_recarga;
            }

            set
            {
                fecha_recarga = value;
            }
        }
    }
}
